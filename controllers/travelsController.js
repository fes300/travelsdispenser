var express = require('express')
var axios = require('axios')
var errors = require('../constants/errors.json')
var Travel = require('../models/travel')

var app = module.exports = express()

const getBearer = async (req, res) => {
  if (!req.headers.authorization) {
    res.status(401).send(errors.noAuthSpecified)
    return { noAuth: true }
  }
  const token = req.headers.authorization.split('Bearer ')[1]
  const user = await axios({
    method: 'get',
    url: 'http://authapi:4001/users/checkBearer',
    headers: {
      common: {
        Authorization: `Bearer ${token}`
      }
    }
  })
  .then(response => ({ ok: true, user: response.data }))
  .catch(error => ({ ok: false, error }))

  return user
}

async function getTravels (req, res) {
  const response = await getBearer(req, res)
  if (response.noAuth) return
  if (!response.ok) return res.status(206).send(response.error)

  Travel.find({
    'owner.email': response.user.email
  }, ['payload', 'updated'], (err, travels) => {
    if (err) {
      res.send.status(206).send(err)
    } else {
      res.status(201).json(travels)
    }
  })
}

const addTravel = async (req, res) => {
  const response = await getBearer(req, res)
  if (response.noAuth) return
  if (!response.ok) return res.status(206).send(response.error)

  const travel = new Travel({
    owner: response.user,
    payload: req.body,
    version: '1.0'
  })

  travel.save((error) => {
    if (error) return res.status(206).send(error)
    return res.status(201).send({
      message: 'the travel was successfully added.',
      travel: travel.payload,
      success: true
    })
  })
}

const deleteTravel = async (req, res) =>
  new Promise(setTimeout(resolve =>
    resolve(res.status(201).send({ deletedTravel: 'yes' }))
  ), 100)

app.route('/:id?')
  .get(getTravels)
  .delete(deleteTravel)
  .post(addTravel)
