var mongoose = require('mongoose')
var validate = require('mongoose-validator')
var Schema = mongoose.Schema

const emailValidator = validate({
  validator: 'isEmail',
  passIfEmpty: true,
  message: 'The email is not correct.'
})

const travelSchema = new Schema({
  owner: {
    email: {
      type: String,
      required: true,
      validate: emailValidator,
      index: true
    },
    userName: {
      type: String,
      required: true
    }
  },
  version: {
    type: String,
    required: true
  },
  payload: {
    title: {
      type: String,
      index: true
    },
    start: {
      type: Date,
      required: true,
      index: true
    },
    end: {
      type: Date,
      required: true,
      index: true
    }
  },
  created: {
    type: Date,
    default: Date.now
  },
  updated: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('travel', travelSchema)
